import { Component, OnInit } from '@angular/core';
import { ComicsService } from './comics.service';
import { Observable } from 'rxjs/Observable';
import { ComicsInfo } from './comicsinfo';
import { DatePipe } from '@angular/common';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';

@Component({
  selector: 'tp-list-comics',
  templateUrl: './list-comics.component.html',
  styleUrls: ['./list-comics.component.css']
})
export class ListComics implements OnInit {

  //Attributs
  displayedColumns = ["title", "releaseDate", "image"];
  dataSource = new MatTableDataSource();
  comicsList : ComicsInfo[] = [];
  imageHeight : number = 150;
  imageMargin : number = 5;

  constructor(private _comicsService: ComicsService, private router: Router){ }

  //Methodes
  ngOnInit(): void {
    this.refreshListComics();
  }


  private refreshListComics() {
    this._comicsService.getComics()
    .subscribe(
      jsonData => 
        {
          this.comicsList = this._comicsService.parseJsonComicsList(jsonData)
          this.dataSource = new MatTableDataSource(this.comicsList); 

        }
        ,
      error => console.log('error'),
      () => console.log('Liste des comics mise à jour')
    );
  }

  onRowClicked(row){
    let url : string = '/comics/';
    url = url.concat(row.id);

    this.router.navigate([url]);
  }

}