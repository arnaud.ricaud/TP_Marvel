export class ComicsInfo{
    id: number;
    titre :string;
    dateSortie : Date;
    createurs : Array<[string, string]> = []; //Role, Name
    diamondCode : string;
    webSiteUrl: string;
    imageUrl : string;

    constructor(id: number, titre :string, dateSortie : Date, createurs : Array<[string, string]>, diamondCode : string, webSiteUrl: string, imageUrl: string){
        this.id = id;
        this.titre = titre;
        this.dateSortie = dateSortie;
        this.createurs = createurs;
        this.diamondCode = diamondCode;
        this.webSiteUrl = webSiteUrl;
        this.imageUrl = imageUrl;
    }
}