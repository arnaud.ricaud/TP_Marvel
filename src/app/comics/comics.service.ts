import { Injectable, OnInit } from "@angular/core";
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { ComicsInfo } from "./comicsinfo";
import {Observable} from 'rxjs';


@Injectable()
export class ComicsService{

    privateKey : string = '3dd1291ba353253dea96195c0cfacb35f7282516';
    publicKey : string = '3a1d27d3ebfd7097c0b6dd5a067266cf';
    timeStamp : string = '1473236363';
    hash : string = '9945a7b9b2b8145a570e619ab7680592';


    constructor(private _http: HttpClient){}

    getComics(): Observable<any>{
        //Timestamp
        //this.timeStamp = String(Date.now());

        //TODO Calcul du hash
        //this.hash = 

        let requete: string = 'http://gateway.marvel.com/v1/public/comics?';
        requete = requete.concat(
            'apikey=',this.publicKey,
            '&ts=',this.timeStamp,
            '&hash=', this.hash,
            '&format=comic&dateDescriptor=lastWeek'
        )
        return this._http.get(requete);
    }


    parseJsonComicsList(json: any): ComicsInfo[]{
        let comicsList : ComicsInfo[] = [];

        for(let resultIndex in json.data.results){
          let result = json.data.results[resultIndex];
    
          let dateSortie : Date = new Date();
          let creatorsList : Array<[string, string]> = [];
          let urlDetails : string;
    
          // Récupération de la date de sortie
          for(let dateIndex in result.dates){
              let date = result.dates[dateIndex];
            if(date.type == 'onsaleDate'){
                dateSortie = new Date(date.date);
                break;
            }
          }  
    
          // Récupération de la liste des créateurs
          for(let creatorIndex in result.creators.items){
            let creator = result.creators.items[creatorIndex];
            creatorsList.push([creator.role, creator.name]);
          }  
    
          // Recherche de l'URL (siteWeb)
          for(let urlIndex in result.urls){
            let urlInfo = result.urls[urlIndex];
    
            if(urlInfo.type == 'detail'){
              urlDetails = urlInfo.url;
              break;
            }
          }  
    
          //Récupération de l'image
          let imageUrl = result.images[0].path.concat('.',result.images[0].extension);
    
          //Ajout du comics à la liste
          comicsList.push(new ComicsInfo((+result.id), result.title, dateSortie , creatorsList, result.diamondCode, urlDetails, imageUrl))
    
        }
        return comicsList;
      }
}