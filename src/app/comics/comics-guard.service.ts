import { Injectable } from "@angular/core";
import { CanActivate, ActivatedRouteSnapshot, Router } from "@angular/router";


@Injectable()
export class ComicsGuardService implements CanActivate {

    constructor(private router: Router){ }

    canActivate(currentRoute: ActivatedRouteSnapshot): boolean{
        let id: number = +currentRoute.url[1].toString();
        if(isNaN(id)){
            alert('Invalid comics Id');
            this.router.navigate(['/liste-comics']);
            return false;
        }
        return true;
    }
}