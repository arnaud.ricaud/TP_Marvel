import { Component, OnInit } from '@angular/core';
import { ComicsInfo } from './comicsinfo';
import { ComicsService } from './comics.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'tp-comics',
  templateUrl: './comics.component.html',
  styleUrls: ['./comics.component.css']
})
export class Comics implements OnInit{
  comics : ComicsInfo = null;
  idComics : number;
  imageMaxHeight : number = 500;

  constructor(private _comicsService: ComicsService, private _route: ActivatedRoute){
    this.idComics = +this._route.snapshot.paramMap.get('id');
  }

  ngOnInit(): void {
    this._comicsService.getComics()
    .subscribe(
      jsonData => this.getComicsDetails(this._comicsService.parseJsonComicsList(jsonData), this.idComics),
      error => console.log('error'),
      () => console.log('Liste des comics mise à jour')
    );
  }

  getComicsDetails(comicsList : ComicsInfo[], id : number): void{
    this.comics = comicsList.find(function(comics) {
      return comics.id == id;
    });
    console.log('Details du comics mis à jour')
  }
}
