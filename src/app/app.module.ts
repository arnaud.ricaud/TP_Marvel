import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {RouterModule} from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatTableModule } from '@angular/material/table';
import { MatDividerModule } from '@angular/material/divider';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatListModule } from '@angular/material/list';

import { AppComponent }  from './app.component';
import { ListComics }  from './comics/list-comics.component';
import { Comics }  from './comics/comics.component';
import { ComicsService } from './comics/comics.service';
import { ComicsGuardService } from './comics/comics-guard.service';

@NgModule({
	imports:	[ 
		BrowserModule,
		HttpClientModule,
    
    //Angular materials
    BrowserAnimationsModule,
    MatToolbarModule,
		MatTableModule,
		MatButtonModule,
		MatDividerModule,
		MatGridListModule,
		MatListModule,

    //Routes
		RouterModule.forRoot([
				{path:'liste-comics', component: ListComics},
				{path:'comics/:id', canActivate: [ComicsGuardService], component: Comics},
				{path:'', redirectTo:'liste-comics', pathMatch:'full'},
				{path:'**', redirectTo:'liste-comics'},				
			])
	],
	declarations: [ AppComponent, ListComics, Comics ],
	providers: [ComicsService, ComicsGuardService],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }
